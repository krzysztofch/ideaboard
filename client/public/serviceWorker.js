self.addEventListener("push", (event) => { 
    let message = JSON.parse(event.data.text()).message;

    let title = "Gentle reminder";   
    let body = message || "Default message";  
    let tag = "Web-push";
   
    event.waitUntil(
      Promise.all([        
          self.registration.showNotification(title, { body, tag }),
      ])      
    )   
});