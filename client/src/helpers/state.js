state = {
    loginForm: {
        username: String,
        password: String,
        submitted: Boolean
    },
    registrationForm: {
        name: String,
        surname: String,
        username: String,
        password: String,
        confirmPassword: String,
        isPasswordValid: Boolean,
        submitted: Boolean,
    },
    searchForm: {
        searchText: String
    },
    avatarForm: {        
        isAvatarModalOpen: Boolean,
        isFileSelected: Boolean,
        fileName: String,
        submitted: Boolean
    },
    authentication: {
        loginInProgress: Boolean,
        loggedIn: Boolean,
        logInError: String,
        user: {
                id: String,
                isAdmin: Boolean,
                name: String,
                surname: String,
                username: String,
                imgSrc: String       
            },
        csrfToken: String,
    },    
    stickies: {
        isFetching: String,
        stickiesList: [],
        stickyToAdd: {
            isModalOpen: Boolean,            
            column: String,
            message: String
        },
        stickyToUpdate: {
            isModalOpen: Boolean,
            _id: Number,
            message: String,
            lastModified: Date
        },
        boardUpdate: {
            isModalOpen: Boolean,
            message: String
        },
        filterText: String
    },
    notifications: {
        message: String
    },   
    pushNotificationsForm: {
        message: String,           
        submitted: Boolean
    },
    wsNotifications : {
        message: String,
        isModalOpen: Boolean
    }  
}