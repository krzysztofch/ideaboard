import 'abortcontroller-polyfill';

export default function (url, options, timeout = 15000) {
 
    const AbortController = window.AbortController;
    const controller = new AbortController();
    let signal = controller.signal;
   
    var requestOptions = {
        ...options,
        signal
    }
    
    return Promise.race([
        fetch(url, requestOptions),
        new Promise((resolve, reject) =>
            setTimeout(() => {                
                            controller.abort();                                
                            reject(new Error('timeout'));
                       }, timeout)
        )
    ]);
}