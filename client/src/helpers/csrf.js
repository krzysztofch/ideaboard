var requestOptions = { 
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },       
    method: 'POST',
    body: JSON.stringify({ 
        csrfToken: 'token'           
    })        
}

fetch('http://localhost:3000/api/sticky/clearBoard', requestOptions);