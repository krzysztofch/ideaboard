import { loginFormConstants } from '../constants';

export const loginFormActions = {
    submit,
    updateValue
}

function submit () {
    return {
        type: loginFormConstants.SUBMIT_FORM
    }
}

function updateValue (name, value) {
    switch(name) {
        case 'username':
            return {
                type: loginFormConstants.SET_USERNAME,
                payload: {
                    [name]: value
                }
            };
        case 'password':
            return {
                type: loginFormConstants.SET_PASSWORD,
                payload: {
                    [name]: value
                }
            }
    }    
} 