import { avatarFormConstants } from '../constants';
import { userConstants } from '../constants';

import { userService } from '../../services';

export const avatarFormActions = {
    openAvatarUpdateModal,
    closeAvatarUpdateModal,
    updateFile,
    submit,
    sendFile,
    updateAvatarSrc
}

function openAvatarUpdateModal () {
    return {
        type: avatarFormConstants.OPEN_UPDATE_AVATAR_MODAL
    }
}

function closeAvatarUpdateModal () {
    return {
        type: avatarFormConstants.CLOSE_UPDATE_AVATAR_MODAL
    }
}

function updateFile (value) {
    if(value) {
        return {
            type: avatarFormConstants.UPDATE_FILE,
            payload: {  
                isFileSelected: true,          
                fileName: value
            }
        };    
    }  else {
        return {
            type: avatarFormConstants.UPDATE_FILE,
            payload: {  
                isFileSelected: false,          
                fileName: ""
            }
        };
    }  
         
} 

function submit () {
    return {
        type: avatarFormConstants.SUBMIT_FORM
    }
}

function sendFile(file) {
    return dispatch => {
        var formData = new FormData();
        
        formData.append("avatar", file);

        userService.updateAvatar(formData)
            .then(response => {
                if(response.data.status === "ok") {
                    dispatch(closeAvatarUpdateModal());
                    dispatch(updateAvatarSrc(response.data.imgSrc));
                }
            });
    }
}

function updateAvatarSrc (src) {
    return {
        type: userConstants.UPDATE_AVATAR_SRC,
        payload: {  
            imgSrc: src
        }
    };
}