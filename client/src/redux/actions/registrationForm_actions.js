import { registrationFormConstants } from '../constants';

export const registrationFormActions = {
    submit,
    updateValue
}

function submit () {
    return {
        type: registrationFormConstants.SUBMIT_FORM
    }
}

function updateValue (name, value) {
    switch(name) {
        case 'name':
            return {
                type: registrationFormConstants.SET_NAME,
                payload: {
                    [name]: value
                }
            };
        case 'surname':
            return {
                type: registrationFormConstants.SET_SURNAME,
                payload: {
                    [name]: value
                }
            };
        case 'username':
            return {
                type: registrationFormConstants.SET_USERNAME,
                payload: {
                    [name]: value
                }
            };
        case 'password':
            return {
                type: registrationFormConstants.SET_PASSWORD,
                payload: {
                    [name]: value
                }
            }
        case 'confirmPassword':
        return {
            type: registrationFormConstants.SET_CONFIRM_PASSWORD,
            payload: {
                [name]: value
            }
        }
    }    
} 