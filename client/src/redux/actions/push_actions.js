import { pushConstants } from '../constants';

import { pushService } from '../../services';

export const pushActions = {    
    handleChange,
    submit,
    sendPushNotification
}

function handleChange (value) {
    return {
        type: pushConstants.SET_MESSAGE,
        payload: {
            message: value
        }
    }
}

function submit () {    
    return {
        type: pushConstants.SUBMIT_FORM        
    }    
}

function sendPushNotification (user_id, message) { 
    return dispatch => {        
        dispatch(request());

        pushService.sendPushNotification(user_id, message)
            .then(response => {
                console.log(response);
            });
    }  

    function request () { return { type: pushConstants.SEND_NOTIFICATION_REQUEST } }
    function success () { return { type: pushConstants.SEND_NOTIFICATION_SUCCESS } }
    function failure () { return { type: pushConstants.SEND_NOTIFICATION_FAILURE } }
}