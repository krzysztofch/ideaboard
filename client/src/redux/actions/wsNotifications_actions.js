import { wsNoticationsConstants } from '../constants';

export const wsNoticationsActions = {    
    notify,
    closeNotificationModal
}

function notify (value) {
    return {
        type: wsNoticationsConstants.NOTIFY,
        payload: {
            message: value
        }
    }
}

function closeNotificationModal () {
    return {
        type: wsNoticationsConstants.CLOSE_NOTIFICATION_MODAL        
    }
}