import { stickiesConstants } from '../constants';
import { stickiesService } from '../../services';
import { wsService } from '../../services';

export const stickiesActions = {
    getAll,
    addSticky,
    removeSticky,
    openEditStickyModal,
    closeEditStickyModal,
    openAddStickyModal,
    closeAddStickyModal,
    updateValue,
    editSticky,
    clearBoard,
    updateFilterText
};

function getAll () {    
    
    return dispatch => {
        dispatch(getAllReguest());

        stickiesService.getAll()
            .then(response => {                
                if(response.data) {                    
                    dispatch(getAllSuccess(response.data));                    
                } 
            });
    }
    
    function getAllReguest () { return { type: stickiesConstants.REQUEST_ALL } }
    function getAllSuccess (stickies) { return { type: stickiesConstants.REQUEST_ALL_SUCCESS, stickies } }
}

function addSticky (column, message, userId) { 
    
    var temporaryId = Date.now();

    return dispatch => {
        //switch type
        switch(column.replace(/\s/g, "").toLowerCase()) {
            case 'todo':           
                var column_id = 1;                
                break;
            case 'toimprove':            
                var column_id = 2;                
                break;
            case 'wentwell':
                var column_id = 3;               
                break;
        }
       
        dispatch(stickiesActions.closeAddStickyModal());
    
        dispatch(createSticky(column_id, message, userId));
        
        stickiesService.addSticky(column_id, message, userId)
                            .then(response => {
                                dispatch(updateStickyDetails(temporaryId, response.data));
                                wsService.sendNotification(userId);                                     
                            });

        function createSticky (column_id, message, userId) {
            return {
                type: stickiesConstants.ADD_STICKY, 
                payload: [
                    {
                        _id: temporaryId,
                        message: message,
                        column_id: column_id,
                        reporter_id: userId,
                        created: new Date().toISOString(),
                        lastModified: new Date().toISOString()
                    }
                ] 
            }
        }

        function updateStickyDetails (temporaryId, data) {
            return {
                type: stickiesConstants.UPDATE_STICKY_DETAILS,
                payload: {
                    temporaryId,
                    _id: data._id,
                    created: data.created,
                    lastModified: data.lastModified
                }
            }
        }
    }
}

function removeSticky(index, stikcyId, userId) {
    return dispatch => {
        dispatch(removeSticky(index));

        stickiesService.removeSticky(stikcyId)
                            .then(response => {
                                console.log(response);
                                wsService.sendNotification(userId);                                    
                            });

        function removeSticky (index) { return {type: stickiesConstants.REMOVE_STICKY, payload: index } }
    }
}

function openAddStickyModal (column) { 
    //switch type
    switch(column.replace(/\s/g, "").toLowerCase()) {
        case 'todo':           
            var column = 'To Do';
            break;
        case 'toimprove':            
            var column = 'To Improve';
            break;
        case 'wentwell':
            var column = 'Went Well';
            break;
    }

    return {
        type: stickiesConstants.OPEN_ADD_STICKY_MODAL, 
        payload: 
        {            
            column: column           
        }    
    }  
}

function closeAddStickyModal () {
    return {
        type: stickiesConstants.CLOSE_ADD_STICKY_MODAL        
    }
}

function updateValue (value, field) {
    if(field === "newStickyMessage") {
        return {
            type: stickiesConstants.NEW_STICKY_UPDATE_VALUE,
            payload: value
        }
    } else if (field === "updateStickyMessage") {
        return {
            type: stickiesConstants.EDIT_STICKY_UPDATE_VALUE,
            payload: value
        }
    }    
}

function openEditStickyModal (id, message, lastModified) {
    return dispatch => {
        dispatch(editSticky(id, message, lastModified));

        function editSticky (id, message, lastModified) {
             return {
                type: stickiesConstants.OPEN_EDIT_STICKY_MODAL,
                payload:{
                    _id: id,                   
                    message: message,
                    lastModified: lastModified
                } 
            } 
        }
    }
}

function closeEditStickyModal () {
    return {
        type: stickiesConstants.CLOSE_EDIT_STICKY_MODAL        
    }
}

function editSticky (id, message, lastModified, userId) {
    return dispatch => {
        
        //update sticky locally, wait for server response to update lastModified property
        dispatch(editSticky(id, message));

        stickiesService.editSticky(id, message, lastModified)
                            .then(response => {
                                if(response.data.status === "ok") {
                                    dispatch(editStickyUpdateLastModified(id, response.data.lastModified));

                                    wsService.sendNotification(userId);
                                } else {
                                    if (response.data.status === 304){
                                        dispatch(updateBoard('updated'));  
                                    } else if (response.data.status === 404) {
                                        dispatch(updateBoard('removed'));
                                    }
                                                                     
                                    stickiesService.getAll()
                                        .then(response => {
                                            console.log(response);
                                            if(response.data) {                                                
                                                setTimeout(function () { dispatch(updateAllSuccess(response.data)) },2000);                    
                                            } 
                                        });                                    
                                }                                
                            });

        function editSticky (id, message) { 
            return {
                type: stickiesConstants.EDIT_STICKY,
                payload: {
                    _id: id,
                    message: message                   
                } 
            }
        }

        function editStickyUpdateLastModified (id, lastModified) { 
            return {
                type: stickiesConstants.EDIT_STICKY_UPDADE_LAST_MODIFIED,
                payload: {
                    _id: id,
                    lastModified: lastModified                   
                } 
            }
        }

        function updateBoard(type) {
            if (type === 'updated') {
                return {                    
                    type: stickiesConstants.UPDATE_BOARD_UPDATED,
                    payload: {
                        message: "Sticky has been updated by other user"
                    } 
               }
            } else if (type === 'removed') {
                return {  
                    type: stickiesConstants.UPDATE_BOARD_REMOVED,
                    payload: {
                        message: "Sticky has been already removed by other user"
                    } 
                }
            }
             
        }

        function updateAllSuccess (stickies) { return { type: stickiesConstants.UPDATE_ALL_SUCCESS, stickies } }
    }   
}

function clearBoard (token, userId) {
    return dispatch => {        

        stickiesService.clearBoard(token)
                            .then(response => {
                                if(response.data.status === "ok") {
                                    dispatch(clearBoard()); 
                                    
                                    wsService.sendNotification(userId);
                                }                                                             
                            });

        function clearBoard () { return {type: stickiesConstants.CLEAR_BOARD } }
    }
}

function updateFilterText (value) {
    return {
        type: stickiesConstants.UPDATE_FILTER_TEXT,
        payload: {
            text: value
        }       
    }
}