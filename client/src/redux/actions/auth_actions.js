import { userConstants } from '../constants';
import { notificationsConstants } from '../constants';
import { stickiesConstants } from '../constants';
import { authService } from '../../services';

export const authActions = {
    login,
    logout,
    register   
}
 
function login (username, password, history, pathname) {
    return dispatch => {
        dispatch(request({ username }));

        authService.login(username, password)
            .then(response => {                
                if(response.data.loggedIn) {
                                       
                    dispatch(success(response.data.user, response.data.csrfToken));
                    authService.setCookie(response.data.user);                                       
                    
                    //let admin user access register page directly by url
                    if(!response.data.user.isAdmin || pathname !== '/register') {                                              
                        history.push('/home');
                    }                   
                } else {
                    authService.clearCookie();
                    dispatch(failure(response.data.error));
                }
            })
            .catch(err => {     
                dispatch(failure('Request timed out'));        
                console.log(err);
            });
    }
    
    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }

    function success(user, csrfToken) { 
        return { 
            type: userConstants.LOGIN_SUCCESS,
            payload: {
                user: user, 
                csrfToken: csrfToken 
            } 
        }
    }

    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout (history) {
    return dispatch => {

        authService.logout().then(resposne => {
            dispatch({ type: userConstants.LOGOUT });
            dispatch({ type: stickiesConstants.CLEAR_STICKY });
            authService.clearCookie();        
            history.push('/login');
        });        
    }    
}

function register (name, surname, username, password, history, isAdmin, id) {
    return dispatch => {

        dispatch(request());
        
        authService.register(name, surname, username, password, isAdmin, id)
            .then(response => {
                if(response.data.status === 200) {
                    dispatch(setNotifiaction('User registered successfully!'));

                    //clear notifications after 5s
                    setTimeout(function(){
                        dispatch(clearNotifiaction());
                    }, 5000)

                    if (response.data.isAdmin) {
                        history.push('/');
                    } else {
                        history.push('/login');
                    }                   
                }
            });        
    } 
    
    function request(user) { return { type: userConstants.REGISTER_REQUEST } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
    function setNotifiaction(message) { return { type: notificationsConstants.SET_NOTIFICATION, payload: {message: message} }}
    function clearNotifiaction() { return { type: notificationsConstants.CLEAR_NOTIFICATION} } 
}