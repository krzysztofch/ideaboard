import { avatarFormConstants } from '../constants';

const initialState = { 
    isAvatarModalOpen: false,
    isFileSelected: false,
    fileName: "",
    submitted: false
};

export function avatarForm (state = initialState, action) {
    switch(action.type) {
        case avatarFormConstants.OPEN_UPDATE_AVATAR_MODAL:
            return {
               ...state,
               isAvatarModalOpen: true 
            }; 
        case avatarFormConstants.CLOSE_UPDATE_AVATAR_MODAL:
            return {
                isAvatarModalOpen: false,
                isFileSelected: false,
                fileName: "",
                submitted: false
            };
        case avatarFormConstants.UPDATE_FILE:
            return {
                ...state,
                isFileSelected: action.payload.isFileSelected,
                fileName: action.payload.fileName
            };
        case avatarFormConstants.SUBMIT_FORM:
            return {
                
            };               
        default: 
            return state;    
    }
}