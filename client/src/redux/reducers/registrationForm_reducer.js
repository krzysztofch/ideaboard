import { registrationFormConstants } from '../constants';

const initialState = {
    name: '',
    surname: '',
    username: '',
    password: '',
    confirmPassword: '',
    isPasswordValid: false,
    submitted: false 
};

export function registrationForm (state = initialState, action) {
    switch(action.type) {
        case registrationFormConstants.SET_NAME:
            return {
                ...state,
                name: action.payload.name
            };
        case registrationFormConstants.SET_SURNAME:
            return {
                ...state,
                surname: action.payload.surname
            };
        case registrationFormConstants.SET_USERNAME:
            return {
                ...state,
                username: action.payload.username
            };
        case registrationFormConstants.SET_PASSWORD:
            return {
                ...state,
                password: action.payload.password,
                isPasswordValid: action.payload.password === state.confirmPassword ? true : false
            };
        case registrationFormConstants.SET_CONFIRM_PASSWORD:
            return {
                ...state,
                confirmPassword: action.payload.confirmPassword,
                isPasswordValid: action.payload.confirmPassword === state.password ? true : false
            };        
        case registrationFormConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: true
            };        
        default: 
            return state;    
    }
}