import { stickiesConstants } from '../constants';

const initialState = {
    isFetching: false,
    stickiesList: [],
    stickyToAdd: {
        isModalOpen: false,
        column: '',
        message: ''        
    },
    stickyToUpdate: {
        isModalOpen: false,
        _id: null,       
        message: '',
        lastModified: null        
    },
    boardUpdate: {
        isModalOpen: false,
        message: ""
    },
    filterText: ""
};

export function stickies (state = initialState, action) {
    switch(action.type) {
        case stickiesConstants.REQUEST_ALL:
            return {
                ...state,
                isFetching: true
            };
        case stickiesConstants.REQUEST_ALL_SUCCESS:
            return {
                ...state,
                isFetching: false,
                stickiesList: action.stickies                
            };
        case stickiesConstants.CLEAR_STICKY:
            return {
                    ...state,                    
                    stickiesList: []                
                };
        case stickiesConstants.OPEN_ADD_STICKY_MODAL:
            return {
                ...state,
                stickyToAdd: {
                    ...state.stickyToAdd,
                    isModalOpen: true, 
                    column: action.payload.column                                      
                }

            };
        case stickiesConstants.CLOSE_ADD_STICKY_MODAL:
            return {
                ...state,
                stickyToAdd: {
                    column: '',
                    isModalOpen: false, 
                    message: ''                                     
                }
            };
        case stickiesConstants.NEW_STICKY_UPDATE_VALUE:
            return {
                ...state,
                stickyToAdd: {
                    ...state.stickyToAdd,
                    message: action.payload                                     
                }
            };                
        case stickiesConstants.ADD_STICKY:
            return {
                ...state,
                stickiesList: state.stickiesList.concat(action.payload)
            };
        case stickiesConstants.UPDATE_STICKY_DETAILS:
            {
                let newState = { ...state };
                let newStickiesList = newState.stickiesList;
                
                //find index sticky to update
                var stickyToUpdateIndex = state.stickiesList.findIndex(function (sticky) {
                    return sticky._id === action.payload.temporaryId;
                });

                //update value
                newStickiesList[stickyToUpdateIndex]._id = action.payload._id;
                newStickiesList[stickyToUpdateIndex].created = action.payload.created;
                newStickiesList[stickyToUpdateIndex].lastModified = action.payload.lastModified;

                return {
                    ...state,
                    stickiesList: newStickiesList,
                }
            }                    
        case stickiesConstants.REMOVE_STICKY:
            {   
                let newState = { ...state };
                let newStickiesList = newState.stickiesList;
                newStickiesList.splice(action.payload, 1);
                return {
                    ...state,
                    stickiesList: newStickiesList
                } 
            }
        case stickiesConstants.OPEN_EDIT_STICKY_MODAL:               
            return {
                ...state,
                stickyToUpdate: {
                    isModalOpen: true,
                    _id: action.payload._id,
                    message: action.payload.message,
                    lastModified: action.payload.lastModified                                     
                }
            };
        case stickiesConstants.CLOSE_EDIT_STICKY_MODAL:               
            return {
                ...state,
                stickyToUpdate: {
                    isModalOpen: false,
                    _id: null,
                    message: "",
                    lastModified: null                                     
                }
            };            
        case stickiesConstants.EDIT_STICKY_UPDATE_VALUE:
            return {
                ...state,
                stickyToUpdate: {
                    ...state.stickyToUpdate,
                    message: action.payload                                     
                }
            };
        case stickiesConstants.EDIT_STICKY:
            {
                let newState = { ...state };
                let newStickiesList = newState.stickiesList;
                
                //find index sticky to update
                var stickyToUpdateIndex = state.stickiesList.findIndex(function (sticky) {
                    return sticky._id === action.payload._id;
                });            
                //update value
                newStickiesList[stickyToUpdateIndex].message = action.payload.message;

                return {
                    ...state,
                    stickiesList: newStickiesList,
                    stickyToUpdate: {
                        isModalOpen: false,
                        _id: null,
                        message: ""                                      
                    }
                }
            }
        case stickiesConstants.EDIT_STICKY_UPDADE_LAST_MODIFIED:
            {
                let newState = { ...state };
                let newStickiesList = newState.stickiesList;
                
                //find index sticky to update
                var stickyToUpdateIndex = state.stickiesList.findIndex(function (sticky) {
                    return sticky._id === action.payload._id;
                });            
                //update lastModified
                newStickiesList[stickyToUpdateIndex].lastModified = action.payload.lastModified;
                
                return {
                    ...state,
                    stickiesList: newStickiesList                
                }
            }
        case stickiesConstants.UPDATE_BOARD_UPDATED:            
            return {
                ...state,
                boardUpdate: {
                    isModalOpen: true,
                    message: action.payload.message
                }
            };            
        case stickiesConstants.UPDATE_BOARD_REMOVED:            
            return {
                ...state,
                boardUpdate: {
                    isModalOpen: true,
                    message: action.payload.message
                }
            };            
        case stickiesConstants.UPDATE_ALL_SUCCESS:        
            return {
                ...state,
                stickiesList: action.stickies,
                boardUpdate: {
                    isModalOpen: false,
                    message: ""                                        
                }
            };                
        case stickiesConstants.CLEAR_BOARD: 
            return {
                ...state,
                stickiesList: []
            };       
        case stickiesConstants.UPDATE_FILTER_TEXT: 
            return {
                ...state,
                filterText: action.payload.text
            };        
        default: 
            return state;    
    }
}