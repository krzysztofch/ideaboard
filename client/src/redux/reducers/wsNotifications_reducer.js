import { wsNoticationsConstants } from '../constants';

const initialState = {
    message: '',
    isModalOpen: '' 
};

export function wsNotifications (state = initialState, action) {
    switch(action.type) {
        case wsNoticationsConstants.NOTIFY:
            return { 
                message: action.payload.message,
                isModalOpen: true
            }; 
        case wsNoticationsConstants.CLOSE_NOTIFICATION_MODAL:
            return {
                message: "",
                isModalOpen: false
            }      
        default: 
            return state;    
    }
}