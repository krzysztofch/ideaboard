import { userConstants } from '../constants';

const initialState = { 
    loginInProgress: false,
    loggedIn: false,
    logInError:"",
    user: "",   
    csrfToken: ""
};

export function authentication (state = initialState, action) {
    switch(action.type) {
        case userConstants.LOGIN_REQUEST:
            return {
                ...state,
                loginInProgress: true,
                logInError: ""
            };
        case userConstants.LOGIN_SUCCESS:
            return {
                loginInProgress: false,
                loggedIn: true,
                logInError:"",
                user: action.payload.user,
                csrfToken: action.payload.csrfToken
            };
        case userConstants.LOGIN_FAILURE:
            return {
                ...state,
                loginInProgress: false,
                logInError: action.error
            }
        case userConstants.LOGOUT:
            return {
                ...state,
                loggedIn: false,                
                user: "",
                csrfToken: ""
            };
        case userConstants.UPDATE_AVATAR_SRC: 
            return {
                ...state,
                user: {
                    ...state.user,
                    imgSrc: action.payload.imgSrc
                }
            };        
        default: {           
            return state;    
        }
            
    }
}