import { notificationsConstants } from '../constants';

const initialState = { 
    message: ""
};

export function notifications (state = initialState, action) {
    switch(action.type) {
        case notificationsConstants.SET_NOTIFICATION:
            return {
                message: action.payload.message
            };
        case notificationsConstants.CLEAR_NOTIFICATION:
            return {
               message: ""
            };
        default: 
            return state;    
    }
}