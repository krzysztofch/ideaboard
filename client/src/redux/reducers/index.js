import { combineReducers } from 'redux';
import { loginForm } from './loginForm_reducer';
import { registrationForm } from './registrationForm_reducer';
import { authentication } from './authentication_reducer';
import { avatarForm } from './avatarForm_reducer';
import { stickies } from './stickies_reducer';
import { notifications } from './notifications_reducer';
import { pushNotificationsForm } from './push_reducer';
import { wsNotifications } from './wsNotifications_reducer';

const rootReducer = combineReducers({
    loginForm,
    registrationForm,
    authentication,
    avatarForm,
    stickies,
    notifications,
    pushNotificationsForm,
    wsNotifications
});

export default rootReducer;