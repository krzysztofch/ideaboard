import { loginFormConstants } from '../constants';

const initialState = {
    username: '',
    password: '',
    submitted: false 
};

export function loginForm (state = initialState, action) {
    switch(action.type) {
        case loginFormConstants.SET_USERNAME:
            return {
                ...state,
                username: action.payload.username
            };
        case loginFormConstants.SET_PASSWORD:
            return {
                ...state,
                password: action.payload.password
            };
        case loginFormConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: true
            };        
        default: 
            return state;    
    }
}