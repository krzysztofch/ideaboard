import { pushConstants } from '../constants';

const initialState = {
    message: '',           
    submitted: false
};

export function pushNotificationsForm (state = initialState, action) {
    switch(action.type) {
        case pushConstants.SET_MESSAGE:
            return { 
                ...state,
                message: action.payload.message                
            }; 
        case pushConstants.SUBMIT_FORM:
            return {
                ...state,
                submitted: true
            }      
        default: 
            return state;    
    }
}