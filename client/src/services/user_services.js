import fetch from '../helpers/fetchWithTimeout.js'

export const userService = {
    updateAvatar
};

function updateAvatar (data) {
    
    const requestOptions = {        
        credentials: 'include',
        method: 'POST',
        body: data        
    }
    
    return fetch('./api/user/updateAvatar', requestOptions)
                .then((response) => {                         
                    return response.json();
                })     
                .catch((error) => {  
                    console.log(error); 
                });      
}