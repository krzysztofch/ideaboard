import { wsNoticationsActions } from '../redux/actions';

export const wsService = {
    openConnection,
    sendNotification
};

//webSocket Instance
var webSocket;

function openConnection (id, dispatch) {
    webSocket = new WebSocket("ws://localhost:5000/websocket");

    var user_id = id;
    webSocket.onmessage = function (event) {    
        var data = JSON.parse(event.data);
        
        if(!(data.user_id === user_id)) {       
            dispatch(wsNoticationsActions.notify(data.message));        
        }
    }
}

function sendNotification (id) {    
    var payload = {
        user_id: id
      };
    
    webSocket.send(JSON.stringify(payload));    
}