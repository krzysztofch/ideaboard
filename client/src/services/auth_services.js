import fetch from '../helpers/fetchWithTimeout.js'

export const authService = {
    setCookie,
    clearCookie,
    checkLogInStatus,    
    login,
    logout,
    register    
};

function login (username, password) {
    
    const requestOptions = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify({ 
            username,
            password 
        })
    }
    
    return fetch('/api/authenticate', requestOptions)
                .then((response) => {                         
                    return response.json();
                })     
                .catch((error) => {  
                    console.log(error);                 
                    throw new Error("timeout");
                });      
}

function logout () {
    const requestOptions = {        
        credentials: 'include',
        method: 'GET'        
    }
    
    return fetch('/api/authenticate/logout', requestOptions)
                .then((response) => {                         
                    return response.json();
                })     
                .catch((error) => {  
                    console.log(error);                 
                    throw new Error("timeout");
                });      
}

function register (name, surname, username, password, isAdmin, id) {   
    const requestOptions = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        method: 'POST',
        body: JSON.stringify({ 
            name,
            surname,
            username,
            password, 
            isAdmin,
            _id: id             
        })
    }    
    
    return fetch('/api/authenticate/register', requestOptions)
                .then((response) => {       
                    return response.json();
                })     
                .catch((error) => {
                    console.log(error);
                });      
}

function setCookie (cookieValue) {
        var expiresDays = 7;
        var cookieName = 'user=';
        var date = new Date();
        date.setTime(date.getTime() + (expiresDays*24*60*60*1000));
        var expires = "expires=" + date.toGMTString();
        document.cookie = cookieName + JSON.stringify(cookieValue) + ";" + expires + ";path=/";
}

function clearCookie () {   
    var cookieName = 'user=';   
    document.cookie = cookieName + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
}

function checkLogInStatus () {  
        var user;  
        var cookieName = 'user=';
        var decodedCookie = decodeURIComponent(document.cookie);
        var cookieTable = decodedCookie.split(';');
        for(var i = 0; i < cookieTable.length; i++) {
            var cookie = cookieTable[i];
            while (cookie.charAt(0) === ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(cookieName) === 0) {
                user =  cookie.substring(cookieName.length, cookie.length);
            }
        }
      
       if (user) {
           return JSON.parse(user);
       } else {
           return null;
       }        
}