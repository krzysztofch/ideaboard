import fetch from '../helpers/fetchWithTimeout.js'

export const stickiesService = {
    getAll,
    removeSticky,
    editSticky,
    addSticky,
    clearBoard    
};

function getAll () {

    const requestOptions = { 
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        method: 'GET',
        credentials: 'include'      
    }

    return fetch('/api/sticky/getAll', requestOptions)
            .then((response) => {       
                return response.json();
            })     
            .catch((error) => {
                console.log(error);
            });      
}

function removeSticky (stickyId) {
    return fetch('api/sticky/delete/' + stickyId, {
                    method: 'DELETE',
                    credentials: 'include',
                })
                .then((response) => {       
                    return response.json();
                })     
                .catch((error) => {
                    console.log(error);
                }); 
}

function editSticky (stickyId, message, lastModified) {
    const requestOptions = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify({ 
            stickyId,
            message,
            lastModified
        })
    }

    return fetch('api/sticky/editSticky', requestOptions)
                .then((response) => {       
                    return response.json();
                })     
                .catch((error) => {
                    console.log(error);
                }); 
}

function addSticky (column_id, message, userId) {
    const requestOptions = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify({ 
            message,
            column_id,           
            reporter_id: userId           
        })
    }
    
    return fetch('/api/sticky/addSticky', requestOptions)
                .then((response) => {       
                    return response.json();
                })     
                .catch((error) => {
                    console.log(error);
                });     
}

function clearBoard (token) {  
    
    const requestOptions = { 
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }, 
        credentials: 'include',      
        method: 'POST',
        body: JSON.stringify({ 
            csrfToken: token           
        })        
    }

    return fetch('/api/sticky/clearBoard', requestOptions)
            .then((response) => {       
                return response.json();
            })     
            .catch((error) => {
                console.log(error);
            });      
}