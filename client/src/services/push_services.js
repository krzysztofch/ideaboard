import fetch from '../helpers/fetchWithTimeout.js'

export const pushService = {
    postSubscriptionDetails,
    sendPushNotification    
};

function postSubscriptionDetails(subscription) {
        
    var sub = JSON.parse(JSON.stringify(subscription));  
    
    var userSubscription = {
        endpoint: sub.endpoint,
        p256dh: sub.keys.p256dh,
        auth: sub.keys.auth
    };

    return fetch('/api/push/registerSubscriber', {
                method: 'POST',
                headers: new Headers(
                    {
                        'Content-Type': 'application/json'
                    }
                ),
                body: JSON.stringify(userSubscription)
            }).then((response) => {       
                return response.json();
            })     
            .catch((error) => {
                console.log(error);
            });            
}

function sendPushNotification (user_id, message) {
    
    const body = {
        message: message,
        user_id: user_id
    }

    return  fetch('/api/push/sendNotification', {
                method: 'POST',
                headers: new Headers(
                    {
                        'Content-Type': 'application/json'
                    }
                ),
                body: JSON.stringify(body)          
            })
            .then((response) => {       
                return response.json();
            })     
            .catch((error) => {
                console.log(error);
            }); 
}