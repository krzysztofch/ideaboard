import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';

import { PrivateRoute } from './components/HOC/PrivateRoute';
import  HomePage from './components/views/HomePage';
import  NotifyPage  from './components/views/NotifyPage';
import  RegisterPage from './components/views/RegisterPage';
import  LoginPage from './components/views/LoginPage';
import Footer from './components/presentational/Footer';
import { HeaderContainer } from './components/containers/HeaderContainer';

import { pushService } from './services';

class App extends Component { 

  componentDidMount() {  
    if ("Notification" in window) {

      Notification.requestPermission()
        .then(sendPushSubscription);
      
      function sendPushSubscription (permission) {
          if (permission === 'denied') {                
            console.log('You have denied notifications.');
          } else if (navigator.serviceWorker && permission === 'granted') {             
              var applicationServerPublicKey = "BJljqgZ7lRV2TinLrycKaFYRhKQROFf_1KhKgn-y7hTWZXWSWOeCA4dwL0MZzsctLNOrE7lol1Ft_SPNe3M2phI";
        
              //register service worker is done by calling regsterServiceWorker.js in index.js
              //navigator.serviceWorker.register('/serviceWorker.js');
                
              navigator.serviceWorker.ready
                  .then(function(registration) {
                          const subscribeOptions = {
                              userVisibleOnly: true,
                              applicationServerKey: urlBase64ToUint8Array(applicationServerPublicKey)
                          }; 
    
                          return registration.pushManager.subscribe(subscribeOptions);
                  })
                  //save subscription on server
                  .then(function (pushSubscription) { 
                    console.log(pushSubscription);                                               
                    return pushService.postSubscriptionDetails(pushSubscription);                     
                  })
                  .then(function(response) {
                    if(response.data.status === "ok") {                      
                      console.log('Push notifications have been subscribed');
                    }                     
                  })
                  .catch(function(e){
                    console.log('Error during subscription: ', e);                
                  })
            } 
      }    
    
      function urlBase64ToUint8Array(base64String) {
          const padding = '='.repeat((4 - base64String.length % 4) % 4);
          const base64 = (base64String + padding)
              .replace(/\-/g, '+')
              .replace(/_/g, '/')
          ;
          const rawData = window.atob(base64);
  
          return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)));
      }
    } 
  }

  render() {
    const { loggedIn } = this.props;
    const { isAdmin } = this.props.loggedInUser;

    return (
      <div className="app">
        <div className="wrapper">        
          <Router>          
              <React.Fragment>
                  <HeaderContainer />
                  <Route path="/login" component={LoginPage} />
                  <Route path="/register" component={RegisterPage} />
                  <PrivateRoute path="/home" component={HomePage} requirements={[loggedIn]} />     
                  <PrivateRoute exact path="/notify" component={NotifyPage} requirements={[loggedIn, isAdmin]} />
              </React.Fragment>                 
          </Router>
        </div>
        <Footer footerText="React & ExpressJS project" />
      </div>           
    )
  }
}

function mapStateToProps ({ authentication }) {
  return {
      loggedIn: authentication.loggedIn,
      loggedInUser: authentication.user      
  };
}

var connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };