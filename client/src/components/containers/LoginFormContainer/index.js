import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { loginFormActions, authActions } from '../../../redux/actions';

import LoginForm from '../../presentational/LoginForm';

class LoginFormContainer extends Component { 
    
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChange(e) {        
        const { name, value } = e.target;
        const { dispatch } = this.props;
        dispatch(loginFormActions.updateValue(name, value));
    }

    handleSubmit(e) {       
        e.preventDefault();     
        
        const { loginForm, dispatch } = this.props;
        const { username, password, submitted } = loginForm;
        
        if (!submitted) {
            dispatch(loginFormActions.submit());
        }

        if (username && password) {
            dispatch(authActions.login(username, password, this.props.history));
        }
    }  

    render() {

        const { authentication, loginForm, location } = this.props; 

        const { loginInProgress, logInError } = authentication;
        const { username, password, submitted } = loginForm;

        return (
            <LoginForm  username={username}
                        password={password}
                        submitted={submitted}
                        onSubmit={this.handleSubmit}
                        onChange={this.handleChange}
                        loginInProgress={loginInProgress} 
                        logInError={logInError}
                        location={location} /> 
        );
    }
}

function mapStateToProps ({ authentication, loginForm }) {
    return {
        authentication: authentication,
        loginForm: loginForm       
    };
}

var connectedLoginFormContainer = connect(mapStateToProps)(LoginFormContainer);
const connectedLoginFormContainerWithRouter = withRouter(connectedLoginFormContainer); 
export { connectedLoginFormContainerWithRouter as LoginFormContainer };