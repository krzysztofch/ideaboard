import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { authActions } from '../../../redux/actions';
import { stickiesActions } from '../../../redux/actions';

import Header from '../../presentational/Header';

class HeaderContainer extends Component { 
    
    constructor(props) {
        super(props);
       
        this.handleLogout = this.handleLogout.bind(this);        
        this.handleClearBoard = this.handleClearBoard.bind(this);
    }

     handleLogout(e) {
        e.preventDefault(); 
        const { dispatch } = this.props;  
          
        dispatch(authActions.logout(this.props.history));        
    }    

    handleClearBoard(){
        const { dispatch, authentication } = this.props;
        const { user, csrfToken } = authentication;
        
        dispatch(stickiesActions.clearBoard(csrfToken, user.id));
    }

    render() {

        const { authentication } = this.props; 

        return (
            <Header isLogoutButton = {authentication.loggedIn} 
                    handleLogout = {authentication.loggedIn ? this.handleLogout : null} 
                    headerText = 'Idea Board' 
                    isAdminUser = {authentication.user ? authentication.user.isAdmin : false}                            
                    handleClearBoard = {(authentication.user && authentication.user.isAdmin) ? this.handleClearBoard : null}                         
                    location = {this.props.location.pathname} />
        )
    }
}

function mapStateToProps ({ authentication }) {
    return {
        authentication: authentication
    };
}

var connectedHeaderContainer = connect(mapStateToProps)(HeaderContainer);
const connectedHeaderContainerWithRouter = withRouter(connectedHeaderContainer); 
export { connectedHeaderContainerWithRouter as HeaderContainer }; 