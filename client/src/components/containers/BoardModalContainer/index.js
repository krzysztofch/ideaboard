import React, { Component } from 'react';
import { connect } from 'react-redux';

import { wsNoticationsActions, stickiesActions } from '../../../redux/actions';

import Modal from '../../presentational/Modal';

class BoardModalContainer extends Component { 
    
    constructor(props) {
        super(props);   
        
        this.handleCloseNotificationModal = this.handleCloseNotificationModal.bind(this);
        this.refreshStickies = this.refreshStickies.bind(this);
    }

    handleCloseNotificationModal() {
        const { dispatch } = this.props;
       
        dispatch(wsNoticationsActions.closeNotificationModal());
    }   

    refreshStickies() {
        const { dispatch } = this.props;

        dispatch(stickiesActions.getAll());
        dispatch(wsNoticationsActions.closeNotificationModal());
    }  

    render () {
        const { wsNotifications, stickies } = this.props;
        const { isModalOpen: isWsNotificationModalOpen } = wsNotifications;
        const { boardUpdate } = stickies;
        const { isModalOpen: isBoardUpdateModalOpen } = boardUpdate;

        return (
            <React.Fragment>
                {isBoardUpdateModalOpen &&  
                    <Modal  type = "boardUpdate"                        
                            textValue = {boardUpdate.message} /> }
        
                {isWsNotificationModalOpen && 
                    <Modal  type = "boardUpdateWS"
                            header = {wsNotifications.message} 
                            submitText= "Refresh stickies"
                            onSubmit = {this.refreshStickies}
                            cancelText = "Dismiss"
                            onCancel = {this.handleCloseNotificationModal} /> }
            </React.Fragment>
        )        
    }
}

function mapStateToProps ({ stickies, wsNotifications }) {
    return {
        stickies: stickies,
        wsNotifications: wsNotifications       
    };
}

var connectedBoardModalContainer = connect(mapStateToProps)(BoardModalContainer);
export { connectedBoardModalContainer as BoardModalContainer };