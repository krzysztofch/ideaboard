import React, { Component } from 'react';
import { connect } from 'react-redux';

import { avatarFormActions } from '../../../redux/actions';
import { stickiesActions } from '../../../redux/actions';

import UserBar from '../../presentational/UserBar';

// create reference to avatarFormElements
const avatarImgRef = React.createRef();
const avatarFileInputRef = React.createRef();

class UserBarContainer extends Component {
    constructor (props) {
        super(props);

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleFileUpdate = this.handleFileUpdate.bind(this);
        this.handleFileSend = this.handleFileSend.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleOpenModal() {
        const { dispatch } = this.props;

        dispatch(avatarFormActions.openAvatarUpdateModal());
    }

    handleCloseModal() {
        const { dispatch } = this.props;

        dispatch(avatarFormActions.closeAvatarUpdateModal());
    }

    handleFileUpdate(ev) {
        const { dispatch } = this.props;

        if (ev.target.files.length) {
            var selectedFile = ev.target.files[0];
            
            dispatch(avatarFormActions.updateFile(selectedFile.name));
           
            var reader = new FileReader();

            reader.onloadend = () => {
                avatarImgRef.current.src = reader.result;
            }

            reader.readAsDataURL(selectedFile);            
        } else {
            dispatch(avatarFormActions.updateFile(""));
        }
        
    }

    handleFileSend() {
        const { dispatch } = this.props;       

        var file = avatarFileInputRef.current.files[0];              
        
        dispatch(avatarFormActions.sendFile(file));
    }

    handleChange(ev) {
        const { value, name } = ev.target;       
        const { dispatch } = this.props;

        if (name === "filerText") {
            dispatch(stickiesActions.updateFilterText(value));
        } 
    }

    render () { 
        const { authentication, filterText, avatarForm } = this.props;
        const { name, imgSrc } = authentication.user;

        return (
            <UserBar    name={name} 
                        imgSrc={imgSrc}
                        filterText={filterText} 
                        avatarForm={avatarForm} 
                        handleOpenModal={this.handleOpenModal} 
                        handleCloseModal={this.handleCloseModal} 
                        handleFileUpdate={this.handleFileUpdate}
                        handleFileSend={this.handleFileSend} 
                        handleChange={this.handleChange} 
                        ref={{  avatarImgRef: avatarImgRef,
                                avatarFileInputRef: avatarFileInputRef }} />
        );       
    }
}

function mapStateToProps ({ authentication, stickies, avatarForm }) {
    return {
        authentication: authentication,
        filterText: stickies.filterText,
        avatarForm: avatarForm        
    };
}

var connectedUserBarContainer = connect(mapStateToProps)(UserBarContainer);
export { connectedUserBarContainer as UserBarContainer }; 