import React, { Component } from 'react';
import { connect } from 'react-redux';

import { wsService } from '../../../services';
import { stickiesActions } from '../../../redux/actions';

import StickiesList from '../../presentational/StickiesList';

// create reference to submit button
const refSubmitButton = React.createRef();

class StickiesListContainer extends Component {
    constructor(props){
        super(props); 
        
        this.handleOpenAddStickyModal = this.handleOpenAddStickyModal.bind(this); 
        this.handleCloseAddStickyModal = this.handleCloseAddStickyModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddSticky = this.handleAddSticky.bind(this);
        this.handleEditSticky = this.handleEditSticky.bind(this);     
        this.handleRemoveSticky = this.handleRemoveSticky.bind(this);
        this.handleOpenEditStickyModal = this.handleOpenEditStickyModal.bind(this);
        this.handleCloseEditStickyModal = this.handleCloseEditStickyModal.bind(this);  
        this.submitWithEnter = this.submitWithEnter.bind(this);           
    }

    componentDidMount(){ 
        const { dispatch, loggedInUser } = this.props;           
        const { id: user_id } = loggedInUser;    

        dispatch(stickiesActions.getAll());

        //setUp websocket connection
        wsService.openConnection(user_id, dispatch);        
    }
           
    handleOpenAddStickyModal(column) {
        const { dispatch } = this.props;

        dispatch(stickiesActions.openAddStickyModal(column));
        
        window.scrollTo(0,0);       

        window.addEventListener("keydown", this.submitWithEnter);
    }

    handleCloseAddStickyModal() {
        const { dispatch } = this.props;

        dispatch(stickiesActions.closeAddStickyModal());

        window.removeEventListener("keydown", this.submitWithEnter);        
    }

    handleAddSticky() {
        const { dispatch, stickies, loggedInUser } = this.props;
        const { stickyToAdd } = stickies;      

        dispatch(stickiesActions.addSticky(stickyToAdd.column, stickyToAdd.message, loggedInUser.id ));
    }

    handleChange(ev) {       
        const { value, name } = ev.target;
        const { dispatch } = this.props;

        dispatch(stickiesActions.updateValue(value, name));              
    }    
     
    handleEditSticky(stickyId) {
        const { dispatch, stickies, loggedInUser } = this.props;
        const { stickyToUpdate } = stickies;

        dispatch(stickiesActions.editSticky(stickyId, stickyToUpdate.message, stickyToUpdate.lastModified, loggedInUser.id));
    }

    handleRemoveSticky(stickyId) {
        const { dispatch, stickies, loggedInUser } = this.props;

        //find index sticky to remove
        var stickyToRemoveIndex = stickies.stickiesList.findIndex(function (sticky) {
                                                                        return sticky._id === stickyId;
                                                                   });

        dispatch(stickiesActions.removeSticky(stickyToRemoveIndex, stickyId, loggedInUser.id ));
    } 
    
    handleOpenEditStickyModal(stickyId) {
        const { dispatch, stickies } = this.props;

        //find index sticky to update
        var stickyToUpdateIndex = stickies.stickiesList.findIndex(function (sticky) {
                                                                        return sticky._id === stickyId;
                                                                   });
        
        var messageToUpdate = stickies.stickiesList[stickyToUpdateIndex].message;
        var lastModified = stickies.stickiesList[stickyToUpdateIndex].lastModified;
        
        dispatch(stickiesActions.openEditStickyModal(stickyId, messageToUpdate, lastModified));

        window.scrollTo(0,0); 

        window.addEventListener("keydown", this.submitWithEnter);
    }

    handleCloseEditStickyModal() {
        const { dispatch } = this.props;

        dispatch(stickiesActions.closeEditStickyModal()); 
        
        window.removeEventListener("keydown", this.submitWithEnter);
    }

    submitWithEnter (ev) {
        if (ev.keyCode === 13) {
            ev.preventDefault();                
            refSubmitButton.current.click();

            window.removeEventListener("keydown", this.submitWithEnter);
        }  
    }    

    render() {
        const { stickies, loggedInUser } = this.props;

        return (
            <StickiesList   stickies={stickies} 
                            loggedInUser={loggedInUser} 
                            handleOpenAddStickyModal={this.handleOpenAddStickyModal} 
                            handleCloseAddStickyModal={this.handleCloseAddStickyModal}
                            handleAddSticky={this.handleAddSticky} 
                            handleChange={this.handleChange} 
                            handleEditSticky={this.handleEditSticky} 
                            handleRemoveSticky={this.handleRemoveSticky} 
                            handleOpenEditStickyModal={this.handleOpenEditStickyModal} 
                            handleCloseEditStickyModal={this.handleCloseEditStickyModal}
                            submitWithEnter={this.submitWithEnter} 
                            refSubmitButton={refSubmitButton} />
        )                                
    }
}

function mapStateToProps ({ authentication, stickies }) {
    return {
        loggedInUser: authentication.user,       
        stickies: stickies   
    };
}

var connectedStickiesListContainer = connect(mapStateToProps)(StickiesListContainer);
export { connectedStickiesListContainer as StickiesListContainer }; 