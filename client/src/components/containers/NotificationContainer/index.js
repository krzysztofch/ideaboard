import React from 'react';
import { connect } from 'react-redux';

import Notification from '../../presentational/Notification';

const NotificationContainer = ({ notifications }) => {    
    const { message: notification } = notifications;  

    return (
        <React.Fragment>
            {notification && <Notification message={notification}/>}
        </React.Fragment>
    )    
}

function mapStateToProps ({ notifications }) {
    return {
        notifications: notifications
    };
}

var connectedNotificationContainer = connect(mapStateToProps)(NotificationContainer);
export { connectedNotificationContainer as NotificationContainer }; 