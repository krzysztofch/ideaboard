import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { registrationFormActions, authActions } from '../../../redux/actions';

import RegisterForm from '../../presentational/RegisterForm';

class RegisterFormContainer extends Component { 
    
    constructor(props) {
        super(props);
        
        //check for react-routing props
        //setInterval(function(){console.log(this.props)}.bind(this),2000);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);        
    }

    handleChange(e) {
        const { name, value } = e.target;
        const { dispatch } = this.props;

        dispatch(registrationFormActions.updateValue(name,value));      
    }

    handleSubmit(e) {
        e.preventDefault();        
        
        const { authentication, registrationForm, dispatch } = this.props;
        const { name, surname, username, password, confirmPassword, submitted, } = registrationForm;
        const { user } = authentication;
        
        let from;
        
        if(this.props.location.state) {
            from = this.props.location.state.from;
        } else {
            from = undefined;
        }       

        var isAdmin = from === '/' ? true : false; 

        if (!submitted) {
            dispatch(registrationFormActions.submit())
        }

        var passwordIsConfirmed = false;

        if(password === confirmPassword) {
            passwordIsConfirmed = true; 
        }

        if (name && surname && username && password && passwordIsConfirmed) {
            if(isAdmin) {
                dispatch(authActions.register(name, surname, username, password, this.props.history, true, user._id));
            } else {                
                dispatch(authActions.register(name, surname, username, password, this.props.history, false));
            }           
        } 
    } 

    render() {

        const { registrationForm, location } = this.props; 
        const { name, surname, username, password, confirmPassword, isPasswordValid, submitted } = registrationForm;

        return (            
            <RegisterForm   name={name}
                            surname={surname}
                            username={username}
                            password={password}
                            confirmPassword={confirmPassword}
                            isPasswordValid={isPasswordValid}
                            submitted={submitted}
                            onChange={this.handleChange} 
                            onSubmit={this.handleSubmit} 
                            location={location} />      
        );
    }
}

function mapStateToProps ({ authentication, registrationForm }) {
    return {
        authentication: authentication,
        registrationForm: registrationForm        
    };
}

var connectedRegisterFormContainer = connect(mapStateToProps)(RegisterFormContainer);
const connectedRegisterFormContainerWithRouter = withRouter(connectedRegisterFormContainer); 
export { connectedRegisterFormContainerWithRouter as RegisterFormContainer };
