import React, { Component } from 'react';
import { connect } from 'react-redux';

import { pushActions } from '../../../redux/actions';

import NotifyForm from '../../presentational/NotifyForm';

class NotifyFormContainer extends Component { 
    
    constructor(props) {
        super(props);   
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (e) {
        const { value } = e.target;
        const { dispatch } = this.props;

        dispatch(pushActions.handleChange(value));
    }

    handleSubmit (e) {
        e.preventDefault();

        const { authentication, pushNotificationsForm, dispatch } = this.props;
        const { user } = authentication;
        const { message } = pushNotificationsForm;

        dispatch(pushActions.submit()); 

        if (message) {            
            dispatch(pushActions.sendPushNotification(user.id, message));
        }                
    }

    render() {
        const { pushNotificationsForm } = this.props;
        const { message, submitted } = pushNotificationsForm;
        
        return (
            <NotifyForm message={message}
                        submitted={submitted}
                        onChange={this.handleChange}
                        onSubmit={this.handleSubmit} />
        )
    }
}

function mapStateToProps ({ authentication, pushNotificationsForm }) {
    return {
        authentication: authentication,
        pushNotificationsForm: pushNotificationsForm
    };
}

var connectedNotifyFormContainer = connect(mapStateToProps)(NotifyFormContainer);
export { connectedNotifyFormContainer as NotifyFormContainer }; 