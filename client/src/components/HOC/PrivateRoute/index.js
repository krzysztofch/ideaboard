import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { authService } from '../../../services';
import { authActions } from '../../../redux/actions';


class PrivateRoute extends Component {

    componentDidMount() {            
        const { dispatch } = this.props;
       
        var currentLoggedInUser = authService.checkLogInStatus();
        
        if(currentLoggedInUser) {            
            dispatch(authActions.login(currentLoggedInUser.username, null, this.props.history, this.props.location.pathname));         
        }        
    }

    render() { 
        console.log('iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', this.props);      
        const { component: Component, requirements, ...rest } = this.props;
        
        const authorized = requirements.every(function(element) {
            return element === true;
        });
       
        return(
            <Route {...rest} render = {props => (
                authorized
                    ? <Component {...props} />
                    : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            )} />
        )
    }
}

const connectedPrivateRoute = connect()(PrivateRoute);
const connectedPrivateRouteWithRouter = withRouter(connectedPrivateRoute); 
export { connectedPrivateRouteWithRouter as PrivateRoute };