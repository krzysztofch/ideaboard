import React from 'react';

import { RegisterFormContainer } from '../../containers/RegisterFormContainer';

const RegisterPage = () => {
    return (            
        <React.Fragment> 
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <RegisterFormContainer />                                                                        
                    </div>
                </div>
            </div>            
        </React.Fragment>    
    );    
}

export default RegisterPage;