import React from 'react';

import { NotifyFormContainer } from '../../containers/NotifyFormContainer';

const NotifyPage = () => { 
    return (            
        <React.Fragment> 
            <div className="container">
                <div className="row"> 
                    <NotifyFormContainer /> 
                </div>
            </div>
        </React.Fragment>
    );    
}

export default NotifyPage;