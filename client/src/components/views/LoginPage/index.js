import React from 'react';

import { NotificationContainer } from '../../containers/NotificationContainer';
import { LoginFormContainer } from '../../containers/LoginFormContainer';

const LoginPage = (props) => { 
    return (
        <React.Fragment>
            <div className="container">
                <div className="row">               
                    <div className="col-md-6 col-md-offset-3">
                        <NotificationContainer/>
                        <LoginFormContainer />                          
                    </div>
                </div>
            </div>            
        </React.Fragment>
    )    
}

export default LoginPage;