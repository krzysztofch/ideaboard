import React from 'react';
import {  Route, Link } from 'react-router-dom';

import { UserBarContainer } from '../../containers/UserBarContainer';
import { NotificationContainer } from '../../containers/NotificationContainer';
import { StickiesListContainer } from '../../containers/StickiesListContainer';
import { BoardModalContainer } from '../../containers/BoardModalContainer';

const HomePage = (props) => { 
    console.log(props)
    return (            
        <React.Fragment>
            <UserBarContainer /> 
            <div className="container">
                <div className="row">
                    <NotificationContainer />
                    <StickiesListContainer />  
                </div>         
            </div>
            <BoardModalContainer />   
            <Link className="link" to="/home/route">new route</Link>
            <Route path="/home/route" render ={(props)=>(<div>"cant/get/access/here"</div>)} />                    
        </React.Fragment> 
    );    
}

export default HomePage;