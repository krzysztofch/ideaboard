import React from 'react';

import './styles.css';

const Notification = ( { message } ) => { 
    return (
        <div className="notification-wrapper"><span>{message}</span></div>
    ); 
}

export default Notification;