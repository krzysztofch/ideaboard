import React from 'react';

import './styles.css';

const NotifyForm = ({submitted, message, onChange, onSubmit}) => {
    return (
        <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <form name="form" onSubmit={onSubmit}>
                            <div className={'form-group' + (submitted && !message ? ' has-error' : '')}>
                                <textarea   className="textarea-notification" 
                                            value={message} 
                                            placeholder="Type your notification here!"
                                            onChange = {onChange} />
                                {submitted && !message &&
                                    <div className="help-block">Notification's message is required</div>
                                }
                            </div>
                            <div className="form-group">
                                <input className="btn btn-primary" type="submit" value="Send notification" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    );    
}

export default NotifyForm;