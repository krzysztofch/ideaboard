import React from 'react';
import { Link } from 'react-router-dom';

import './styles.css';

const RegisterForm = ({ name, surname, username, password, confirmPassword, isPasswordValid, submitted, onChange, onSubmit, location }) => {
    
    let from;
    if(location.state) {
        from = location.state.from;
    } else {
        from = undefined;
    }   

    return (
        <div>                
            {from === "/" ? <h2>Register admin user</h2> : <h2>Register</h2>}
            <form name="form" onSubmit={onSubmit}>
                <div className={'form-group' + (submitted && !name ? ' has-error' : '')}>
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" name="name" value={name} onChange={onChange} autoComplete="off" />
                    {submitted && !name &&
                        <div className="help-block">Name is required</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !surname ? ' has-error' : '')}>
                    <label htmlFor="surname">Surname</label>
                    <input type="text" className="form-control" name="surname" value={surname} onChange={onChange} autoComplete="off" />
                    {submitted && !surname &&
                        <div className="help-block">Surname is required</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                    <label htmlFor="username">Username</label>
                    <input type="text" className="form-control" name="username" value={username} onChange={onChange} autoComplete="off" />
                    { submitted && !username &&
                        <div className="help-block">Username is required</div> }
                </div>
                <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" name="password" value={password} onChange={onChange} autoComplete="off" />
                    {submitted && !password &&
                        <div className="help-block">Password is required</div>
                    }
                </div>
                <div className={'form-group' + (((submitted && !confirmPassword) || (password && submitted && !isPasswordValid)) ? ' has-error' : '')}>
                    <label htmlFor="password">Confirm password</label>
                    <input type="password" className="form-control" name="confirmPassword" value={confirmPassword} onChange={onChange} autoComplete="off" />
                    { submitted && !confirmPassword &&
                        <div className="help-block">Confirm your password</div> }
                    { password && submitted && !isPasswordValid &&
                        <div className="help-block">Passwords don't match</div> }
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-primary">Register</button>                        
                </div>
            </form>
            Back to {" "} 
            {from === "/" ? <Link className="link" to="/">Main page</Link> : <Link className="link" to="/login">Login</Link>}
        </div>
    );
}

export default RegisterForm;