import React from 'react';

import './style.css';

const UserBar = React.forwardRef((props, ref) => {  
    
    const { name, imgSrc, filterText, avatarForm, handleOpenModal, handleCloseModal,
            handleFileUpdate, handleFileSend, handleChange } = props;
    const { isAvatarModalOpen } = avatarForm;
    
    return (
        <React.Fragment>
            <div className="userBar">
                <input type="text" name="filerText" className="filter-input" 
                    value={filterText} placeholder="Search sticky!" onChange={handleChange} />
                <div className="avatar-container">
                    <p className="welcome-text">Welcome { name } </p>          
                    <img  className="avatar" src={imgSrc} onClick={handleOpenModal} alt="avatar" />                       
                    <p className="avatar-hint">Click to change your avatar</p>                           
                </div>                
            </div>

            {isAvatarModalOpen && 
                <div className='modal-container modal-container--avatar'>
                    <div className="modal-overlay">               
                    </div>
                    <div className="modal-window">
                        <div className='modal-header'>
                        Update avatar:
                        </div>
                        <div className="modal-body">
                            <input type="file" id="fileInput" name="fileInput" accept=".jpg, .jpeg, .png" onChange={handleFileUpdate} ref={ref.avatarFileInputRef}/>   
                            <label htmlFor="fileInput" className="avatar-label">
                                <i className="fas fa-plus-circle"></i>
                                &nbsp; Select file
                            </label>
                            <img src="" className="avatar-min" ref={ref.avatarImgRef} alt="avatar" />      
                            <div className="clearFix"></div>                         
                            <button className="close-modal-button" onClick={handleCloseModal}>Cancel changes</button>
                            <button className="save-changes-button" onClick={handleFileSend}>Save changes</button>
                        </div>
                    </div>
                </div>
            }
        </React.Fragment>
    ); 
});

export default UserBar; 