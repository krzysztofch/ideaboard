import React from 'react';

import './styles.css';

const Modal = React.forwardRef((props, ref) => {  

    const { type, header, fieldName, textValue, onChange, submitText, onSubmit, cancelText, onCancel } = props; 

    return (            
        <div className='modal-container'>
            { type !== "boardUpdateWS" &&
                <div className="modal-overlay">               
                </div> }
            <div className={"modal-window" + (type === "boardUpdate" ? " board-updated-modal" : "") + (type === "boardUpdateWS" ? "--push-modal" : "")}>
                <div className={"modal-header" + (type === "boardUpdate" ? " board-updated-header" : "")}>
                    {header}
                </div>
                <div className="modal-body">
                    {(type === "manageSticky") && 
                        <React.Fragment>
                            <textarea name={fieldName} value={textValue} onChange={onChange}/>
                            <button className="close-modal-button" onClick={onCancel}>{cancelText}</button>
                            <button className="save-changes-button" onClick={onSubmit} ref={ref}>{submitText}</button> 
                        </React.Fragment>
                    }
                    {(type === "boardUpdate") &&
                        <React.Fragment>
                            {textValue}
                            <div className="spinner board-update"></div>
                        </React.Fragment>
                    }
                    {(type === "boardUpdateWS") &&
                        <React.Fragment>
                            <button className="close-push-modal-button" onClick={onCancel}>{cancelText}</button>
                            <button className="refresh-stickies-button" onClick={onSubmit}>{submitText}</button>
                        </React.Fragment>
                    }
                </div>
            </div>
        </div>
    );         
})

export default Modal;