import React from 'react';

import './styles.css';

import Sticky from '../Sticky';
import Modal from '../Modal';

const stickiesList = (props) => {  

    const { stickies, loggedInUser, handleOpenAddStickyModal, handleCloseAddStickyModal, handleAddSticky, handleChange,
            handleEditSticky, handleRemoveSticky, handleOpenEditStickyModal, handleCloseEditStickyModal, refSubmitButton } = props;

    const { stickyToAdd, stickyToUpdate,  filterText } = stickies; 
    const { isModalOpen: isAddStickyModalOpen } = stickies.stickyToAdd;
    const { isModalOpen: isUpdateStickyModalOpen } = stickies.stickyToUpdate;

    function groupStickies (stickiesArray, column_id) {
        return stickiesArray
                .filter(sticky => sticky.column_id === column_id)
                .filter(sticky => sticky.message.toLowerCase().includes(filterText.toLowerCase()))
                .map(sticky => (<Sticky key={sticky._id} 
                                        user_id={loggedInUser.id}
                                        reporter_id={sticky.reporter_id}
                                        isAdmin = {loggedInUser.isAdmin} 
                                        removeSticky={handleRemoveSticky.bind(null, sticky._id)}
                                        editSticky={handleOpenEditStickyModal.bind(null, sticky._id)}>{sticky.message}
                                </Sticky>));
    }

    let toDoStickies = groupStickies(stickies.stickiesList, 1);    
    let toImproveStickies = groupStickies(stickies.stickiesList, 2);
    let wentWellStickies = groupStickies(stickies.stickiesList, 3);

    return (
        <React.Fragment>
            <div className="col-xs-12 col-sm-4 sticky-column">
                <div className='columns-header'>
                    <span>TO DO</span>
                    <p><i className="fas fa-plus-square" onClick={handleOpenAddStickyModal.bind(null, 'toDo')}></i></p>                                
                </div>
                <div className='column-stickies'>
                    {toDoStickies}
                </div>
            </div>
            <div className="col-xs-12 col-sm-4 sticky-column">
                <div className='columns-header'>
                    <span>TO IMPROVE</span>
                    <p><i className="fas fa-plus-square" onClick={handleOpenAddStickyModal.bind(null, 'toImprove')}></i></p> 
                </div>
                <div className='column-stickies'>
                    {toImproveStickies}
                </div>
            </div>
            <div className="col-xs-12 col-sm-4 sticky-column">
                <div className='columns-header'>
                    <span>WENT WELL</span>
                    <p><i className="fas fa-plus-square" onClick={handleOpenAddStickyModal.bind(null, 'wentWell')}></i></p>  
                </div>
                <div className='column-stickies'>
                    {wentWellStickies}
                </div>                    
            </div>

            { isAddStickyModalOpen &&
                <Modal  type = "manageSticky"
                        header = {`Add new sticky to ${stickyToAdd.column} column:`} 
                        fieldName = "newStickyMessage"
                        textValue = {stickyToAdd.message} 
                        onChange = {handleChange} 
                        submitText= "Add sticky"
                        onSubmit = {handleAddSticky}
                        cancelText = "Close Modal"
                        onCancel = {handleCloseAddStickyModal} 
                        ref  = {refSubmitButton} /> }                           

            { isUpdateStickyModalOpen && 
                <Modal  type = "manageSticky"
                        header = "Update sticky:"
                        fieldName = "updateStickyMessage" 
                        textValue = {stickyToUpdate.message} 
                        onChange = {handleChange} 
                        submitText= "Save changes"
                        onSubmit = {handleEditSticky.bind(null, stickyToUpdate._id)}
                        cancelText = "Cancel changes"
                        onCancel = {handleCloseEditStickyModal} 
                        ref  = {refSubmitButton} /> } 
        </React.Fragment>
    )
} 

export default stickiesList;