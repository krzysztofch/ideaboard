import React from 'react';

import './styles.css';

const Footer = ({ footerText }) => {         
    return (
        <footer className="app-footer">
            <span>{footerText}</span>
        </footer>
    );       
};

export default Footer;