import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './styles.css';

class Header extends Component {
    constructor(props){
        super(props);      
        
        this.handleResize = this.handleResize.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);

        this.state = {
            mobileMenu: false,
            mobileMenuExpanded: false
        }              
    }

    componentDidMount () {
        window.onresize = this.handleResize;

        if(window.innerWidth < 800) {
            this.setState({
                mobileMenu: true
            });
        }    
    }

    handleResize () {        
        if(window.innerWidth < 800) {
            this.setState({
                mobileMenu: true
            });
        } else {
            this.setState((prevState, props) => {
                return {
                    mobileMenu: false, 
                    mobileMenuExpanded: false
                };
            });
        }       
    }

    toggleMenu () {
        this.setState((prevState, props) => {
            return { 
                mobileMenuExpanded: !prevState.mobileMenuExpanded
            };
        });
    }

    render () {
        
        const { isLogoutButton, handleLogout, headerText, isAdminUser, handleClearBoard, location, ref } = this.props; 
       
        let { mobileMenu, mobileMenuExpanded } = this.state;

        let showMenu = ( isLogoutButton || isAdminUser ) ? true : false;

        if (mobileMenu) {
            return (
                <header className="app-header">
                    <h1 className="app-title mobile">{ headerText } </h1>
                    {showMenu && <i className="fas fa-bars hamburger-menu" onClick={this.toggleMenu}></i>}
                    {(showMenu && mobileMenuExpanded) && (
                        <div className="mobile-menu">
                            <ul>
                                { isAdminUser && location !== '/notify' && <li><a onClick={handleClearBoard}>Clear board</a></li>} 
                                { isAdminUser && location !== '/notify' && <li><Link to={{ pathname: '/notify' }}>Send notification</Link> </li> }                              
                                { isAdminUser && location !== '/notify' && <li><Link to={{ pathname: '/register', state: { from: this.props.location } }}>Add admin user</Link></li> } 
                                { location === '/notify' && <li><Link to={{ pathname: '/', state: { from: this.props.location }}}>Back to homepage</Link></li> }  
                                { isLogoutButton && <li><a onClick={handleLogout}>Log out</a></li>}                                    
                            </ul>
                        </div>
                    )}                                        
                </header>
            )
        } else {
            return (            
                <header className="app-header">                
                    { isAdminUser && location !== '/notify' && <button type="button" id="clear-board-button" className="menu-button menu-button--left" onClick={handleClearBoard}>Clear board</button> } 
                    { isAdminUser &&  location !== '/notify' && <Link to={{ pathname: '/notify' }} className="menu-button menu-button--left link-button">Send notification</Link> }
                    { location === '/notify' && <Link to={{ pathname: '/', state: { from: this.props.location }}} className="menu-button menu-button--left link-button">Back to homepage</Link> }
                    { isLogoutButton && <a id="logout-button" className="menu-button menu-button--right" onClick={handleLogout}>Log out</a>}
                    { isAdminUser && location !== '/notify' && <Link to={{ pathname: '/register', state: { from: location } }} id="add-admin-button" className="menu-button menu-button--right">Add admin user</Link> } 
                    <h1 className="app-title">{ headerText } {ref}</h1>                    
                </header>
            ); 
        }    
    }
}

export default Header;