import React from 'react';

import './style.css';

const Sticky = ( {user_id, reporter_id, sticky_id, isAdmin, removeSticky, editSticky, children} ) => {             
    return (
        <div className="sticky">
            {((user_id === reporter_id) || isAdmin) && (
            <div>
                <span className="sticky__deleteButton" onClick={removeSticky}><i className="fas fa-times"></i></span>
                <span className="sticky__editButton" onClick={editSticky.bind(null, sticky_id)}><i className="fas fa-pencil-alt"></i></span>
            </div>
            )}
            {children}
        </div>
    ); 
}

export default Sticky;