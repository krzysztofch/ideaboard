import React from 'react';
import { Link } from 'react-router-dom';

import './styles.css';

const LoginForm = ( { username, password, submitted, onSubmit, onChange, loginInProgress, logInError, location } ) => {    
    return (
        <div>        
            <h2>Log in</h2>                                                       
            <div className="errors">
                {logInError}
            </div>
            <form name="form" onSubmit={onSubmit}>
                <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                    <label htmlFor="username">Username</label>
                    <input type="text" className="form-control" name="username" value={username} onChange={onChange} autoComplete="off"/>
                    {submitted && !username &&
                        <div className="help-block">Username is required</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" name="password" value={password} onChange={onChange} autoComplete="off"/>
                    {submitted && !password &&
                        <div className="help-block">Password is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary">Login</button>                        
                </div>
            </form>
            Not registered yet? {" "}
            <Link className="link" to={{ pathname: '/register', state: { from: location.pathname } }}>Register</Link>
            { loginInProgress && <div className="spinner"></div> }
        </div> 
    );    
}

export default LoginForm; 