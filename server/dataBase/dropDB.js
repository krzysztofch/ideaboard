const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/ideaBoard');

mongoose.connection.on('open', function(){
    mongoose.connection.db.dropDatabase(function (err) {
        console.log('database dropped');
        process.exit(0);
      });
})