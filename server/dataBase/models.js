const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    surname: String,
    username: String,
    password: String,
    isAdmin: Boolean,
    imgSrc: String    
});

var stickySchema = new mongoose.Schema({    
    message: String,
    column_id: Number,
    reporter_id: { type: Schema.Types.ObjectId, ref: 'User' },
    created: { type: Date, default: Date.now },
    lastModified:  { type: Date, default: Date.now }
});

var subscriptionSchema = new mongoose.Schema({    
    endpoint: String,
    p256dh: String,
    auth: String           
})

var User = mongoose.model('User', userSchema);
var Sticky = mongoose.model('Sticky', stickySchema);
var Subscription = mongoose.model('Subscription', subscriptionSchema);

module.exports = {
    User: User,   
    Sticky: Sticky,
    Subscription: Subscription
}