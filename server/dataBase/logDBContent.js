const mongoose = require('mongoose');
const async = require('async');
const Models = require('./models'); 

const { User, Sticky, Subscription } = Models;

const port = 5000;

mongoose.connect('mongodb://localhost/ideaBoard');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

async.parallel({
    users: function (cb) {
        User.find({}, cb)
    },       
    stickies: function (cb) {
        Sticky.find({}, cb)
    },
    subscriptions:  function (cb) {
        Subscription.find({}, cb)
    },
}, function (err, results) {
        console.log(results);        
        process.exit(0);
});