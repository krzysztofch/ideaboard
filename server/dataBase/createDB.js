const mongoose = require('mongoose');
const Models = require('./models.js');

const { User, Sticky, Subscription } = Models;

//create database if not exist 
var connect = mongoose.connect('mongodb://localhost/ideaBoard');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var user1 = new User({
    _id: new mongoose.Types.ObjectId(),
    name: 'user1',
    surname: 'user1',
    username: 'user1@test.com',
    password: 'asdf',
    isAdmin: false,
    imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
});

var user2 = new User({
    _id: new mongoose.Types.ObjectId(),
    name: 'user2',
    surname: 'user2',
    username: 'user2@test.com',
    password: 'asdf',
    isAdmin: true,
    imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
});

var user3 = new User({
    _id: new mongoose.Types.ObjectId(),
    name: 'user3',
    surname: 'user3',
    username: 'user3@test.com',
    password: 'asdf',
    isAdmin: false,
    imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
});

var user4 = new User({
    _id: new mongoose.Types.ObjectId(),
    name: 'admin',
    surname: 'admin',
    username: 'admin@test.com',
    password: 'asdf',
    isAdmin: true,
    imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
});
  
user1.save(function (err) {
    if (err) throw new Error(err);

    var sticky1 = new Sticky({
        message: 'Write super application',
        column_id: 2,
        reporter_id: user1._id,
        created: Date.now(),
        lastModified: Date.now()
    });

    sticky1.save(function (err) {
        if (err) throw new Error(err);      
    });

    var sticky2 = new Sticky({
        message: 'Check if everything is working ok',
        column_id: 1,
        reporter_id: user1._id,
        created: Date.now(),
        lastModified: Date.now()
    });
    ;
    sticky2.save(function (err) {
        if (err) throw new Error(err);       
    });
});

user2.save(function (err) {
    if (err) throw new Error(err);

    var sticky1 = new Sticky({
        message: 'Improve team velocity',
        column_id: 3,
        reporter_id: user2._id,
        created: Date.now(),
        lastModified: Date.now()
    });

    sticky1.save(function (err) {
        if (err) throw new Error(err);      
    });
    
});

user3.save(function (err) {
    if (err) throw new Error(err);

    var sticky1 = new Sticky({
        message: 'Determine next sprint scope',
        column_id: 2,
        reporter_id: user3._id,
        created: Date.now(),
        lastModified: Date.now()
    });
    
    sticky1.save(function (err) {
        if (err) throw new Error(err);              
    });
      
});

user4.save(function (err){
    if (err) throw new Error(err); 
});

console.log('Database created');