const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors'); 
const cookieParser = require('cookie-parser');
const path = require('path');
const session = require('express-session');
const uuid = require('uuid/v4');
const express_Ws = require('express-ws');

const app = express();

const expressWs = express_Ws(app);

var authenticateController = require('./controllers/authenticateController.js');
var userController = require('./controllers/userController.js');
var stickyController =  require('./controllers/stickyController.js');
var pushController =  require('./controllers/pushController.js');
var websocketController = require('./controllers/websocketController');

//add WS clients to request
app.use( function (req, res, next) {
    req.wsClients = expressWs.getWss('/').clients;
    next();
});

app.use(cors({
    origin: 'http://localhost:3000',
    credentials: true
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());

app.use(session({
    genid: (req) => {      
      return uuid();
    },
    secret: 'keyboard cat',
    saveUninitialized: false,
    cookie: {
        maxAge: 24*60*60*1000 
    }
})); 

app.use('/api/authenticate', authenticateController);
app.use('/api/user', userController);
app.use('/api/sticky', stickyController);
app.use('/api/push', pushController);
app.use('/websocket', websocketController);

const port = 5000;

mongoose.connect('mongodb://localhost/ideaBoard');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

app.listen(port, () => { console.log(`Listening on port ${port}`)});