var express = require('express');
const mongoose = require('mongoose');
const Models = require('../dataBase/models.js');
const uuid = require('uuid/v4');

var router = express.Router();

const { User, Sticky } = Models;

router.post('/', (req, res) => {
   
    //check connect.sid cookie on request
    if(req.cookies['connect.sid']) {       
        //retrive session if exist
        if (req.session.loggedIn) {                   
            res.send({
                data: {
                    user: {
                            name: req.session.user.name,
                            surname: req.session.user.surname, 
                            username: req.session.user.username,
                            id: req.session.user.id,
                            isAdmin: req.session.user.isAdmin,
                            imgSrc: req.session.user.imgSrc
                        },                
                    loggedIn: true,
                    csrfToken: req.session.csrfToken      
                }
            });
        } else {

            res.clearCookie("connect.sid");

            res.send({
                data: {
                    error: 'Your session expired. Please log in.',
                    loggedIn: false        
                }
            });
        }
    } else {        
        const username = req.body.username;
        const password = req.body.password;   
    
        User.findOne({'username': username}, function (err, result){
            if (err) throw new Error(err);
    
            if (result) {
                if(result.password == password) {                                        
                    req.session.loggedIn = true;
                    req.session.user = {
                                        name: result.name,
                                        surname: result.surname, 
                                        username: result.username, 
                                        id: result._id,
                                        isAdmin: result.isAdmin,
                                        imgSrc: result.imgSrc
                                        };  
                    
                    //generate csrfToken
                    const csrfToken = uuid();
                    req.session.csrfToken = csrfToken;  
                                            
                    res.send({
                        data: {
                            user: {
                                    name: result.name, 
                                    surname: result.surname,
                                    username: result.username,
                                    id: result._id,                                   
                                    isAdmin: result.isAdmin,
                                    imgSrc: result.imgSrc
                                },
                            loggedIn: true,
                            csrfToken: csrfToken      
                        }
                    });
                } else {
                    res.send({
                        data: {
                            error: 'Wrong password',
                            loggedIn: false        
                        }
                    });
                }
            } else {
                res.send({
                    data: {
                        error: 'No registered user',
                        loggedIn: false        
                    }
                });
            }        
        });
    };       
});

router.post('/register', (req, res) => {      
    const name = req.body.name;
    const surname = req.body.surname;
    const username = req.body.username;
    const password = req.body.password;
    
    //check if request is send by admin user
    if(req.session.user && req.session.user.isAdmin) {  

        var newUser = new User({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            surname: surname,
            username: username,
            password: password,
            isAdmin: true,
            imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
        });
    
        newUser.save(function (err, user) {
            if (err) throw new Error(err);
            
            res.send({
                data: {
                    status: 200,
                    isAdmin: true       
                }
            });
        });

    } else {
        var newUser = new User({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            surname: surname,
            username: username,
            password: password,
            isAdmin: false,
            imgSrc: "/images/26aae639-af0c-457a-a956-39f36af79094default.png"
        });
    
        newUser.save(function (err, user) {
            if (err) throw new Error(err);
    
            res.send({
                data: {
                    status: 200,
                    isAdmin: false       
                }
            });
        });
    }
});

router.get('/logout', (req, res) => {    
   req.session.destroy(function (err) {
        if (err) throw new Error(err);
       
        res.clearCookie("connect.sid");

        res.send({
            data: {
                status: 200               
            }
        });
    });    
});

module.exports = router;