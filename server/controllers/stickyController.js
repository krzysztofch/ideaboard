var express = require('express');
const mongoose = require('mongoose');
const fetch = require('node-fetch');
const Models = require('../dataBase/models.js');

var router = express.Router();

const { User, Sticky } = Models;

router.get('/getAll', (req, res) => {
   if(req.session.loggedIn) {
    Sticky.find({}, function (err, results) {
        res.send({ data: Array.from(results)});
     }); 
   } else {
        res.send({status: '403 Not authorized'});
   }        
});

router.delete('/delete/:id', (req, res) => { 
    if(req.session.loggedIn) {

        var id = req.params.id; 

        const user_id = req.session.user.id;

        Sticky.findOne({ _id: id }, function (err, sticky) {
            if (err) throw new Error(err); 
            
            if(sticky && (req.session.user.id == sticky.reporter_id || req.session.user.isAdmin)) {
                sticky.remove(function (err) {
                    if(err) throw new Error(err);                    

                    res.send({ data: "ok" });
                });                
            } else {
                res.send({status: '403 Not authorized user'});
            }
            
          });

    } else {
        res.send({status: '403 Not authorized access'});
    }         
});

router.post('/addSticky', (req, res) => {

    if(req.session.loggedIn) {   
        const message = req.body.message;
        const column_id = req.body.column_id;
        const reporter_id = req.body.reporter_id;

        const user_id = req.session.user.id;

        const created = Date.now();
        const lastModified = Date.now();

        var newSticky = new Sticky({
            message: message,
            column_id: column_id,
            reporter_id: reporter_id,
            created: created,
            lastModified: lastModified
        });
        
        newSticky.save(function (err, newSticky) {
            if (err) throw new Error(err);            

            res.send({ data: {
                        _id: newSticky._id,
                        created: newSticky.created,
                        lastModified: newSticky.lastModified,
                        } 
            });        
        });
    } else {        
        res.send({status: "403 Not authorized"});        
    }

}); 

router.post('/editSticky', (req, res) => {

    if(req.session.loggedIn) { 

        const stickyId = req.body.stickyId;    
        const message = req.body.message;
        const lastModified =  new Date(req.body.lastModified).getTime();
        
        const user_id = req.session.user.id;

        Sticky.findById(stickyId, function (err, sticky) {
            if (err) throw new Error(err);      
            
            if (sticky) {

                if (req.session.user.id == sticky.reporter_id || req.session.user.isAdmin) {
                    if (sticky.lastModified.getTime() == lastModified) {
                        sticky.message = message;
                        var newLastModified = new Date();
                        sticky.lastModified = newLastModified;
            
                        sticky.save(function (err) {
                            if (err) throw new Error(err);  

                            res.send({ data: {
                                            status: "ok",
                                            lastModified: newLastModified.toISOString()
                                        }
                            });
                        });
                    } else {
                        res.send({ data: {  
                                        status: 304,                              
                                        statusText:  "Sticky has been updated"
                                } 
                        });
                    } 
                } else {
                    res.send({status: "403 Not authorized user"});
                }            
            } else {
                    res.send({ data: {
                            status: 404,                        
                            statusText:  "Sticky has been deleted"
                        } 
                    }); 
            }       
        });
    } else {
        res.send({status: "403 Not authorized"});
    }
}); 

router.post('/clearBoard', (req, res) => {
    
    if(req.session.loggedIn && req.session.user.isAdmin) {

        const csrfToken = req.body.csrfToken;      
        
        if (csrfToken == req.session.csrfToken) {
            const user_id = req.session.user.id;

            Sticky.deleteMany({}, function (err) {
                if (err) throw new Error(err);                

                res.send({ data: {
                                    status: "ok"                            
                                }
                });

            }); 
        } else {
            res.send({ data : {
                            status: '403 Not authorized - invalid token'
                        }
            });
        }        
    } else {
         res.send({ data: {
                        status: '403 Not authorized'
                    }
        });
    }        
 });

module.exports = router;