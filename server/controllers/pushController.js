var express = require('express');
const mongoose = require('mongoose');
var webpush = require('web-push');
const Models = require('../dataBase/models.js');

var router = express.Router();

var keys = require('./config/keys.json');

const vapidPublicKey = keys.vapidKeys.publicKey;
const vapidPrivateKey = keys.vapidKeys.privateKey;

const { Subscription } = Models;

router.post('/registerSubscriber', function (req, res){    

    var p256dh = req.body.p256dh;
    var auth = req.body.auth;
    var endpoint = req.body.endpoint;
   
    Subscription.findOne({'endpoint': endpoint}, function (err, result){
        if(!result) {
            var newSubscription = new Subscription({ 
                endpoint: endpoint,        
                p256dh: p256dh,           
                auth: auth        
            });
            
            newSubscription.save(function (err, user) {
                if (err) throw new Error(err);
        
                res.send({
                    data: {
                        status: "ok"             
                    }
                });
            });
        } else {
            res.send({
                data: {
                    status: "ok",
                    message: "already subscribed",      
                }
            });
        }
    });
});

router.post('/sendNotification', function (req, res) { 
    
    const user_id = req.body.user_id;  
    const message = req.body.message;

    var options = {
        TTL: 24 * 60 * 60,
        vapidDetails: {
            subject: 'mailto:kchruscielewski@pgs-soft.com',
            publicKey: vapidPublicKey,
            privateKey: vapidPrivateKey
        }
    };    
    
    var payload = {
        user_id: user_id,
        message: message    
    }
   
    Subscription.find({}, function(err, subscriptionList) {
        
        if (err) throw new Error(err);       
        
        let notificationsNumber = 0; 
        let notificationRegister = [];

        if (subscriptionList.length > 0) {          
            for (let i = 0; i < subscriptionList.length; i++) {
            
                let pushSubscription = {
                    "endpoint":subscriptionList[i].endpoint,
                    "keys": {
                        "p256dh":subscriptionList[i].p256dh,
                        "auth": subscriptionList[i].auth
                        } 
                };
               
                let notification = webpush.sendNotification(pushSubscription, JSON.stringify(payload), options)
                                        .then(function(res) {                        
                                            ++notificationsNumber;                                               
                                        })
                                        .catch(function(err){  
                                            if(err.statusCode.toString().charAt(0) === "4") {
                                                console.log(err); 
                                                console.log('do usuniecia: ', i);
                                                Subscription.findOne({ _id: subscriptionList[i]._id}, function (err, subscription) {
                                                    if (err) throw new Error(err);                                 
                                                    
                                                    subscription.remove(function (err) {
                                                        if(err) throw new Error(err);
                                                    });
                                                });  
                                            }                      
                                        });
                
                notificationRegister.push(notification);                
            }

            Promise.all(notificationRegister).then(function(values){
                res.send({
                    data: {
                        status: "ok",
                        notificationsNumber: notificationsNumber             
                    }
                });
            });            

        } else {
            res.end(); 
        }
        
    }); 
});

module.exports = router;