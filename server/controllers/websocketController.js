var express = require('express');
var bodyParser = require('body-parser');

var router = express.Router();

router.ws('/', function(ws, req) {  
    ws.onmessage = function(msg) {
             
      var payload = {
        user_id: JSON.parse(msg.data).user_id,  
        message: "Board have been updated"
      }

      req.wsClients.forEach(function (client) {
        client.send(JSON.stringify(payload));
      });
    };
});

module.exports = router;