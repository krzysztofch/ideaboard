var express = require('express');
const mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');
const uuid = require('uuid/v4');

const Models = require('../dataBase/models.js');

const { User } = Models;

var router = express.Router();

router.post('/updateAvatar', (req, res, next) => {

    if (req.session.loggedIn) {
        if(req.files) {
            var avatar = req.files.avatar;
    
            var avatarId = uuid();

            avatar.mv(path.join(__dirname, '../public/images/',  avatarId + avatar.name), function(err) {
                if (err) {
                    throw new Error(err); 
                } 
                
                User.findOne({'username': req.session.user.username}, function (err, user) {
                    if (err) {
                        throw new Error(err); 
                    } 

                    var newImgSrc = path.join('images/', avatarId + avatar.name);
                    user.imgSrc = newImgSrc;
                  
                    user.save(function (err) {
                        if (err) {
                            throw new Error(err); 
                        }
                        
                        req.session.user.imgSrc = newImgSrc;

                        res.send({ data: { 
                                        status: "ok",
                                        imgSrc: newImgSrc 
                                   }
                        });
                    })
                    
                });

               
            });
        } else {
            res.send({status: 'No files uploaded'});
        }
        
    } else {
        res.send({status: '403 Not authorized'});
   }   
});

module.exports = router;